import requests
from bs4 import BeautifulSoup
import re

URL = 'https://www.trollandtoad.com/category.php?selected-cat=0&search-words=cursed+necrofear'
URL2 = 'https://www.trollandtoad.com/category.php?selected-cat=0&search-words=one+for+one'

headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36'}

page = requests.get(URL2, headers=headers)

soup=BeautifulSoup(page.content, 'html.parser')


#to get the first one
rows = soup.findAll("div", {"class": "row align-center py-2 m-auto"})
firstrow = soup.find("div", {"class": "row align-center py-2 m-auto"})
price = firstrow.find("div", {"class": "col-2 text-center p-1"})
linktoprice = firstrow.find("img")


#to get all
rows = soup.findAll("div", {"class": "row align-center py-2 m-auto"})
thetuple=[]
thestrings=[]
thefloats=[]
thelinks=[]
for r in rows:
    price = r.find("div", {"class": "col-2 text-center p-1"})
    
    #print(price.getText())
    p = price.getText()
    thestrings.append(p)
    p = p[1:len(p)]
    floatp = (float(p))
    tuple={p,floatp,r.find("img")['src']}
    thetuple.append(tuple)
    thefloats.append(floatp)
    thelinks.append(r.find("img")['src'])
    theprice_pattern = re.compile(r'\$\d+\.\d{1,2}',flags=re.M)
    justtheprice = theprice_pattern.findall(price.text)
    

newtuple = zip(thefloats, thestrings,thelinks)
newtuple.sort(key=lambda tup: tup[0])
for item in newtuple:
    print("Price: "+item[1]+"      website: "+item[2])

mydivs = soup.findAll("div", {"class": "col-2 text-center p-1"})
divstext=""
for div in mydivs:
    divstext+=div.text


#prices = re.findall(r'\$\d+\.\d{1,2}',mydivs.text)
price_pattern = re.compile(r'\$\d+\.\d{1,2}',flags=re.M)
prices = price_pattern.findall(divstext)
#print(prices)



