Author: Joshua Pudrycki


Purpose:  To practice web scraping using Python package Beautiful Soup.
What it does:  This program gathers all prices on the specified webpage, sorts them by price, then lists them along with their respective seller.

Note: program may crash if specified webpage is removed

TODO: The program is currently limited to a single webpage, but could be improved to scrape all pages for a specific card.
